package auth

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/omame/tado-exporter/metrics"
	"golang.org/x/oauth2"
)

const (
	// An Oauth2 token expires after 10 minutes. We need to renew it before then.
	tokenRefreshInterval = 570
)

var (
	Oauth2Conf = &oauth2.Config{
		ClientID:     "tado-web-app",
		ClientSecret: "wZaRN7rpjn3FoNyF5IFuxg9uMzYJcvOoQ8QWiIqS3hfk6gLhVlG57j5YNoZL2Rtc",
		Endpoint: oauth2.Endpoint{
			TokenURL: "https://auth.tado.com/oauth/token",
		},
	}
)

// Context contains the authenticated http client that can
// be used to perform requests to the Tado endpoint
type Context struct {
	Client      *http.Client
	Credentials Credentials
	LastRefresh time.Time
	AuthURL     string
	TokenURL    string
}

// Credentials contains the credentials to access the Tado API
type Credentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

// BuildContext authenticates and returns a context that can be
// used to perform requests to the Tado website
func BuildContext(authURL string, tokenURL string, credentials Credentials) (Context, error) {
	token, err := GetToken(tokenURL, credentials)
	if err != nil {
		return Context{}, fmt.Errorf("failed to get token: %s", err)
	}
	return Context{
		AuthURL:     authURL,
		Client:      Oauth2Conf.Client(context.Background(), token),
		Credentials: credentials,
		LastRefresh: time.Now(),
		TokenURL:    tokenURL,
	}, nil
}

// GetToken gets a new Oauth2 token from the Tado API
func GetToken(tokenURL string, credentials Credentials) (*oauth2.Token, error) {
	form := url.Values{}
	form.Add("client_id", "public-api-preview")
	form.Add("client_secret", "4HJGRffVR8xb3XdEUQpjgZ1VplJi6Xgw")
	form.Add("username", credentials.Username)
	form.Add("password", credentials.Password)
	form.Add("grant_type", "password")
	form.Add("scope", "home.user")

	req, err := http.NewRequest("POST",
		tokenURL+"/oauth/token", strings.NewReader(form.Encode()))
	log.Debugf("setting the token URI to %s", tokenURL+"/oauth/token")
	if err != nil {
		return nil, fmt.Errorf("error creating a new http request to get a new token: %s", err)
	}

	log.Debug("getting a new token")
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(form.Encode())))
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		metrics.TokensErrors.WithLabelValues("POST /oauth/token").Inc()
		return nil, fmt.Errorf("request error sending POST to %s: %s", req.RequestURI, err)
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		metrics.TokensErrors.WithLabelValues(strconv.Itoa(res.StatusCode)).Inc()
		return nil, fmt.Errorf("%s error sending POST to /oauth/token", http.StatusText(res.StatusCode))
	}
	var token *oauth2.Token
	if err := json.NewDecoder(res.Body).Decode(&token); err != nil {
		metrics.TokensErrors.WithLabelValues("decode").Inc()
		return nil, fmt.Errorf("error decoding OAuth2 token: %s", err)
	}
	metrics.TokensAcquired.Inc()
	return token, nil
}

// EnsureTokenIsValid ensures that an auth token hasn't expired and get
func EnsureTokenIsValid(ctx *Context) error {
	timeSinceLastTokenRefresh := time.Now().Sub(ctx.LastRefresh).Seconds()
	if timeSinceLastTokenRefresh > tokenRefreshInterval {
		token, err := GetToken(ctx.TokenURL, ctx.Credentials)
		if err != nil {
			return err
		}
		ctx.Client = Oauth2Conf.Client(context.Background(), token)
		ctx.LastRefresh = time.Now()
	}
	return nil
}
