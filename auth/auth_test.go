package auth_test

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/omame/tado-exporter/auth"
)

var (
	credentials = auth.Credentials{
		Username: "name@surname.org",
		Password: "this-is-not-a-password",
	}
)

func stubTadoServer(t *testing.T) *httptest.Server {
	var apiStub = httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			if r.RequestURI == "/oauth/token" {
				token, err := ioutil.ReadFile("testdata/auth_token.json")
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					t.Error(err)
					return
				}
				w.WriteHeader(http.StatusOK)
				w.Write(token)
			}
		}))
	return apiStub
}

func Test_GetToken(t *testing.T) {
	stubServer := stubTadoServer(t)
	defer stubServer.Close()
	token, err := auth.GetToken(stubServer.URL, credentials)
	if err != nil {
		t.Errorf("Error getting a new token: %v", err)
	}
	if token.AccessToken == "" {
		t.Errorf("The token obtained is invalid")
	}
}

func Test_EnsureTokenIsValid(t *testing.T) {
	stubServer := stubTadoServer(t)
	defer stubServer.Close()
	ctx, err := auth.BuildContext(stubServer.URL, stubServer.URL, credentials)
	if err != nil {
		t.Errorf("Error creating a new context: %v", err)
	}

	ctx.LastRefresh = time.Now().Add(-600 * time.Second)
	err = auth.EnsureTokenIsValid(&ctx)
	if err != nil {
		t.Error("Error refreshing the token")
	}
	if time.Now().Sub(ctx.LastRefresh).Seconds() > 30 {
		t.Error("The token hasn't been refreshed")
	}
}
