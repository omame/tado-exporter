package metrics

import (
	"time"

	"github.com/prometheus/client_golang/prometheus"
)

const (
	namespace = "tado"
)

// Prometheus metrics
var (
	BootTime = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "boot_time_seconds",
		Help:      "unix timestamp of when the service was started",
	})

	ClientErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "client_errors_total",
			Help:      "Number of errors received from the Tado API.",
		}, []string{"reason"})

	ClientRequests = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "client_requests_total",
			Help:      "Number of requests to the Tado API.",
		})

	ExporterUp = prometheus.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "up",
		Help:      "whether the service is ready to receive requests or not",
	})

	Homes = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "home_up",
		Help:      "Homes.",
	}, []string{"home_name"})

	OpenWindowUp = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "open_window_up",
		Help:      "Open window detection (1 = open).",
	}, []string{"home_name", "zone_name"})

	OutsideTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "outside_temperature_celsius",
		Help:      "Outside temperature (in Celsius).",
	}, []string{"home_name"})

	SolarIntensity = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "solar_intensity_percent",
		Help:      "Solar intensity percentage.",
	}, []string{"home_name"})

	TokensAcquired = prometheus.NewCounter(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "tokens_acquired_total",
			Help:      "Number of OAuth2 tokens successfully acquired through the Tado API.",
		})

	TokensErrors = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Name:      "tokens_acquired_errors_total",
			Help:      "Number OAuth2 tokens acquisition errors from the Tado API.",
		}, []string{"error"})

	ZoneActiveUp = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_active_up",
		Help:      "Active state for a zone.",
	}, []string{"home_name", "zone_name", "zone_type"})

	ZoneAwayUp = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_away_up",
		Help:      "Away state for a zone.",
	}, []string{"home_name", "zone_name"})

	ZoneHumidity = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_humidity_percent",
		Help:      "Relative humidity for a zone.",
	}, []string{"home_name", "zone_name", "zone_type"})

	ZoneHeatingPercentage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_heating_percentage",
		Help:      "How much heating power a zone is requesting.",
	}, []string{"home_name", "zone_name", "zone_type"})

	ZoneOverlayUp = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_overlay_up",
		Help:      "Overlay (manual setting) status.",
	}, []string{"home_name", "zone_name"})

	Zones = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_up",
		Help:      "Thermal zones.",
	}, []string{"home_name", "zone_name", "zone_type"})

	ZoneTargetTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_target_temperature_celsius",
		Help:      "Tartet temperature (in Celsius) for a zone.",
	}, []string{"home_name", "zone_name", "zone_type"})

	ZoneTemperature = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "zone_temperature_celsius",
		Help:      "Temperature (in Celsius) for a zone.",
	}, []string{"home_name", "zone_name", "zone_type"})
)

func init() {
	BootTime.Set(float64(time.Now().Unix()))
	ExporterUp.Set(0)
}
