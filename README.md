A Prometheus exporter that scrapes from the Tado API and reports data about your
heating system.

## Doesn't Tado already provide graphs and all that sort of data?

I'm a happy Tado user but I find its monitoring rather underwhelming. I want to
do more with it and I want to automate the heck out of it. For instance, I want
an alert that tells me when to close the windows in the morning before it gets
too hot during summer. I don't want to constantly check my mobile and compare
data in my brain: machines are made for that, not humans.

## I'm sold. So how do you do that?

Alas, there isn't a stable public API nor you can scrape information from within
your home network. There is though an *unofficial* API wich is somewhat
available. It is a bit of a tribal mystery for now as it's undocumented and
rather obfuscated. Nonetheless, I have found
[some documentation](http://blog.scphillips.com/posts/2017/01/the-tado-api-v2/)
which I used to build this.

## What's the status of this project?

Highly experimental, but kinda working. Check out
[metrics/metrics.go](metrics/metrics.go) for the list of available metrics. I
don't have a cooling system so the scope of this project is limited to heating.
