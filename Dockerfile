FROM alpine:3.10

COPY tado-exporter /tado-exporter

RUN apk add --no-cache ca-certificates

EXPOSE 9316

ENTRYPOINT [ "/tado-exporter" ]
