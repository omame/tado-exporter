package main

import (
	"encoding/json"
	"flag"
	"net/http"
	"os"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"gitlab.com/omame/tado-exporter/auth"
	"gitlab.com/omame/tado-exporter/tado"
)

const (
	authURL  = "https://my.tado.com"
	tokenURL = "https://auth.tado.com"
)

func main() {
	var (
		listenAddress = flag.String(
			"web.listen-address", ":9316", "Address to listen on for web interface and telemetry.")
		metricsPath = flag.String(
			"web.telemetry-path", "/metrics", "Path under which to expose metrics.")
		credentialsPath = flag.String(
			"credentials-path", "credentials.json", "Path to the credentials file.")
		debug = flag.Bool(
			"debug", false, "enable debug log level",
		)
	)
	flag.Parse()

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	credentials := loadCredentials(*credentialsPath)
	ctx, err := auth.BuildContext(authURL, tokenURL, credentials)
	if err != nil {
		log.Fatalf("Unable to set up an authentication context: %v", err)
	}
	exporter := &tado.Exporter{
		Ctx: &ctx,
	}
	log.Debugf("tado exporter successfully created")

	prometheus.MustRegister(exporter)

	http.Handle(*metricsPath, promhttp.Handler())
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(`<html>
             <head><title>Tado Exporter</title></head>
             <body>
             <h1>Tado Exporter</h1>
             <p><a href='` + *metricsPath + `'>Metrics</a></p>
             </body>
             </html>`))
	})

	log.Println("tado exporter listening on", *listenAddress)
	log.Fatal(http.ListenAndServe(*listenAddress, nil))
}

func loadCredentials(credentialsPath string) auth.Credentials {
	credentialsFile, err := os.Open(credentialsPath)
	if err != nil {
		log.Fatalf("error opening config file: %s", err)
	}
	var credentials auth.Credentials
	if err := json.NewDecoder(credentialsFile).Decode(&credentials); err != nil {
		log.Fatalf("failed to decode credentials: %s", err)
	}
	return credentials
}
