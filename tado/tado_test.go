package tado_test

import (
	"context"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"regexp"
	"testing"
	"time"

	"gitlab.com/omame/tado-exporter/auth"
	"gitlab.com/omame/tado-exporter/tado"
)

var (
	credentials = auth.Credentials{
		Username: "name@surname.org",
		Password: "this-is-not-a-password",
	}
)

func buildStubContext(authURL string) (*auth.Context, error) {
	token, err := auth.GetToken(authURL, credentials)
	if err != nil {
		return nil, fmt.Errorf("Failed to get a stub token")
	}
	oauth2Conf := auth.Oauth2Conf
	oauth2Conf.Endpoint.AuthURL = authURL + "/oauth/token"
	return &auth.Context{
		AuthURL:     authURL,
		Client:      oauth2Conf.Client(context.Background(), token),
		Credentials: credentials,
		LastRefresh: time.Now(),
	}, nil
}

func serveStubRequest(testdata string, w http.ResponseWriter) {
	body, err := ioutil.ReadFile(fmt.Sprintf("testdata/%s.json", testdata))
	if err != nil {
		fmt.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusOK)
	w.Write(body)
	return
}

func stubTadoServer(t *testing.T, testdata string) *httptest.Server {
	var apiStub = httptest.NewServer(http.HandlerFunc(
		func(w http.ResponseWriter, r *http.Request) {
			switch {
			case r.RequestURI == "/oauth/token":
				serveStubRequest("auth_token", w)
			case r.RequestURI == "/api/v2/me":
				serveStubRequest("GetAccount", w)
			case regexp.MustCompile(`/api/v2/homes/[\d]+$`).MatchString(r.RequestURI):
				serveStubRequest("GetHome", w)
			case regexp.MustCompile(`/api/v2/homes/[\d]+/zones$`).MatchString(r.RequestURI):
				serveStubRequest("GetZones", w)
			case regexp.MustCompile(`/api/v2/homes/[\d]+/zones/[\d]+/state$`).MatchString(r.RequestURI):
				serveStubRequest("GetZoneState", w)
			case regexp.MustCompile(`/api/v2/homes/[\d]+/weather`).MatchString(r.RequestURI):
				serveStubRequest("GetWeather", w)
			}
		}))
	return apiStub
}

func Test_GetAccount(t *testing.T) {
	stubServer := stubTadoServer(t, "GetAccount")
	defer stubServer.Close()
	stubctx, err := buildStubContext(stubServer.URL)
	if err != nil {
		t.Errorf("Failed to build a stub context")
	}
	exporter := tado.Exporter{
		Ctx: stubctx,
	}
	_, err = exporter.GetAccount()
	if err != nil {
		t.Errorf("Error running GetAccount()")
	}
}

func Test_getHome(t *testing.T) {
	stubServer := stubTadoServer(t, "GetHome")
	defer stubServer.Close()
	stubctx, err := buildStubContext(stubServer.URL)
	if err != nil {
		t.Errorf("Failed to build a stub context")
	}
	exporter := tado.Exporter{
		Ctx: stubctx,
	}
	res, err := exporter.GetHome(654321)
	if err != nil {
		t.Error("Error running GetAccount()")
	}
	if res.Name != "My House" {
		t.Error("Wrong house name in getHome()")
	}
}

func Test_getZones(t *testing.T) {
	stubServer := stubTadoServer(t, "GetZones")
	defer stubServer.Close()
	stubctx, err := buildStubContext(stubServer.URL)
	if err != nil {
		t.Errorf("Failed to build a stub context")
	}
	exporter := tado.Exporter{
		Ctx: stubctx,
	}
	res, err := exporter.GetZones(tado.Home{ID: 654321})
	if err != nil {
		t.Error("Error running GetZones()")
	}
	if len(res) != 2 {
		t.Error("Wrong number of zones in getZones()")
	}
	if res[1].Name != "Zone 2" {
		t.Error("Wrong zone name returned")
	}
}

func Test_getZoneState(t *testing.T) {
	stubServer := stubTadoServer(t, "GetZoneState")
	defer stubServer.Close()
	stubctx, err := buildStubContext(stubServer.URL)
	if err != nil {
		t.Errorf("Failed to build a stub context")
	}
	exporter := tado.Exporter{
		Ctx: stubctx,
	}
	res, err := exporter.GetZoneState(
		tado.Home{ID: 654321},
		tado.Zone{ID: 2},
	)
	if err != nil {
		t.Error("Error running GetZoneState()")
	}
	if res.Setting.Temperature.Celsius != 19.5 {
		t.Error("Wrong temperature setting returned")
	}
}

func Test_getWeather(t *testing.T) {
	stubServer := stubTadoServer(t, "GetWeather")
	defer stubServer.Close()
	stubctx, err := buildStubContext(stubServer.URL)
	if err != nil {
		t.Errorf("Failed to build a stub context")
	}
	exporter := tado.Exporter{
		Ctx: stubctx,
	}
	res, err := exporter.GetWeather(654321)
	if err != nil {
		t.Error("Error running getWeather()")
	}
	if res.SolarIntensity.Percentage != 75.9 {
		t.Error("Wrong solar intensity returned")
	}
}
