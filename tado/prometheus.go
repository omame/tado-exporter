package tado

import (
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/omame/tado-exporter/metrics"
)

const (
	namespace = "tado"
)

// Describe implements Collector interface.
func (e *Exporter) Describe(ch chan<- *prometheus.Desc) {
	metrics.BootTime.Describe(ch)
	metrics.ClientErrors.Describe(ch)
	metrics.ClientRequests.Describe(ch)
	metrics.ExporterUp.Describe(ch)
	metrics.Homes.Describe(ch)
	metrics.OpenWindowUp.Describe(ch)
	metrics.OutsideTemperature.Describe(ch)
	metrics.SolarIntensity.Describe(ch)
	metrics.TokensAcquired.Describe(ch)
	metrics.TokensErrors.Describe(ch)
	metrics.ZoneHumidity.Describe(ch)
	metrics.ZoneActiveUp.Describe(ch)
	metrics.ZoneAwayUp.Describe(ch)
	metrics.ZoneHeatingPercentage.Describe(ch)
	metrics.ZoneOverlayUp.Describe(ch)
	metrics.Zones.Describe(ch)
	metrics.ZoneTargetTemperature.Describe(ch)
	metrics.ZoneTemperature.Describe(ch)
}

// Collect collects tado metrics
func (e *Exporter) Collect(ch chan<- prometheus.Metric) {
	account, err := e.GetAccount()
	if err != nil {
		log.Error("failed to fetch the account data: ", err)
		return
	}
	metrics.ExporterUp.Set(1)
	for _, home := range account.Homes {
		tadoHome, err := e.GetHome(home.ID)
		if err != nil {
			log.Error(err, "failed to get home")
			continue
		}
		metrics.Homes.WithLabelValues(home.Name).Set(1)

		tadoZones, err := e.GetZones(tadoHome)
		if err != nil {
			log.Error(err, "failed to get zones")
			continue
		}

		for _, homeZone := range tadoZones {
			zoneState, err := e.GetZoneState(home, homeZone)
			if err != nil {
				log.Error(err, "failed to get zone state")
				continue
			}
			metrics.Zones.WithLabelValues(home.Name, homeZone.Name, homeZone.Type).Set(zoneState.IsOnline())
			metrics.ZoneAwayUp.WithLabelValues(home.Name, homeZone.Name).Set(zoneState.IsAway())
			metrics.ZoneOverlayUp.WithLabelValues(home.Name, homeZone.Name).Set(zoneState.IsOverlay())
			metrics.OpenWindowUp.WithLabelValues(home.Name, homeZone.Name).Set(zoneState.HasOpenWindow())
			metrics.ZoneTemperature.WithLabelValues(home.Name, homeZone.Name, homeZone.Type).Set(zoneState.TemperatureCelsius())
			metrics.ZoneHumidity.WithLabelValues(home.Name, homeZone.Name, homeZone.Type).Set(zoneState.Humidity())
			metrics.ZoneHeatingPercentage.WithLabelValues(home.Name, homeZone.Name, homeZone.Type).Set(zoneState.HeatingPower())
			metrics.ZoneActiveUp.WithLabelValues(home.Name, homeZone.Name, homeZone.Type).Set(zoneState.IsHeatingOn())
			if zoneState.IsHeatingOn() == 1 {
				metrics.ZoneTargetTemperature.WithLabelValues(home.Name, homeZone.Name, homeZone.Type).Set(zoneState.TargetTemperature())
			}
		}

		weather, err := e.GetWeather(home.ID)
		if err != nil {
			log.Error(err, "failed to get weather data")
			continue
		}
		metrics.OutsideTemperature.WithLabelValues(home.Name).Set(weather.OutsideTemperature.Celsius)
		metrics.SolarIntensity.WithLabelValues(home.Name).Set(weather.SolarIntensity.Percentage)
	}

	metrics.BootTime.Collect(ch)
	metrics.ClientErrors.Collect(ch)
	metrics.ClientRequests.Collect(ch)
	metrics.ExporterUp.Collect(ch)
	metrics.Homes.Collect(ch)
	metrics.OpenWindowUp.Collect(ch)
	metrics.OutsideTemperature.Collect(ch)
	metrics.SolarIntensity.Collect(ch)
	metrics.TokensAcquired.Collect(ch)
	metrics.TokensErrors.Collect(ch)
	metrics.ZoneActiveUp.Collect(ch)
	metrics.ZoneAwayUp.Collect(ch)
	metrics.ZoneHeatingPercentage.Collect(ch)
	metrics.ZoneHumidity.Collect(ch)
	metrics.ZoneOverlayUp.Collect(ch)
	metrics.Zones.Collect(ch)
	metrics.ZoneTargetTemperature.Collect(ch)
	metrics.ZoneTemperature.Collect(ch)
}
