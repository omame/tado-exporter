package tado

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/omame/tado-exporter/auth"
	"gitlab.com/omame/tado-exporter/metrics"
)

// Exporter is a Tado exporter
type Exporter struct {
	Ctx *auth.Context
}

// GetAccount returns account data from the Tado API
func (e *Exporter) GetAccount() (ret Account, err error) {
	accountURL := fmt.Sprintf("%s/api/v2/me", e.Ctx.AuthURL)
	buf, err := e.apiRequest(accountURL)
	if err != nil {
		log.Errorf("error in Exporter.GetAccount: %v", err)
		return Account{}, err
	}
	if err = json.Unmarshal(buf, &ret); err != nil {
		log.Errorf("error decoding %s: %v", accountURL, err)
		return Account{}, err
	}
	return
}

// GetHome returns home data from the Tado API
func (e *Exporter) GetHome(id int) (ret Home, err error) {
	homeURL := fmt.Sprintf("%s/api/v2/homes/%d", e.Ctx.AuthURL, id)
	buf, err := e.apiRequest(homeURL)
	if err != nil {
		log.Errorf("error in Exporter.GetHome: %v", err)
		return Home{}, err
	}
	if err = json.Unmarshal(buf, &ret); err != nil {
		log.Errorf("error decoding %s: %v", homeURL, err)
		return Home{}, err
	}
	return
}

// GetZones returns zones data from the Tado API
func (e *Exporter) GetZones(home Home) (ret []Zone, err error) {
	zonesURL := fmt.Sprintf("%s/api/v2/homes/%d/zones", e.Ctx.AuthURL, home.ID)
	buf, err := e.apiRequest(zonesURL)
	if err != nil {
		log.Errorf("error in Exporter.GetZones: %v", err)
		return []Zone{}, err
	}
	if err = json.Unmarshal(buf, &ret); err != nil {
		log.Errorf("error decoding %s: %v", zonesURL, err)
		return []Zone{}, err
	}
	return
}

// GetZoneState returns zone data from the Tado API
func (e *Exporter) GetZoneState(home Home, zone Zone) (ret ZoneState, err error) {
	zoneStateURL := fmt.Sprintf("%s/api/v2/homes/%d/zones/%d/state",
		e.Ctx.AuthURL, home.ID, zone.ID)
	buf, err := e.apiRequest(zoneStateURL)
	if err != nil {
		log.Errorf("error in Exporter.GetZoneState: %v", err)
		return ZoneState{}, err
	}
	if err = json.Unmarshal(buf, &ret); err != nil {
		log.Errorf("error decoding %s: %v", zoneStateURL, err)
		return ZoneState{}, err
	}
	return
}

// GetWeather returns weather data from the Tado API
func (e *Exporter) GetWeather(id int) (ret Weather, err error) {
	weatherURL := fmt.Sprintf("%s/api/v2/homes/%d/weather", e.Ctx.AuthURL, id)
	buf, err := e.apiRequest(weatherURL)
	if err != nil {
		log.Errorf("error in Exporter.GetWeather: %v", err)
		return Weather{}, err
	}
	if err = json.Unmarshal(buf, &ret); err != nil {
		log.Errorf("error decoding %s: %v", weatherURL, err)
		return Weather{}, err
	}
	return
}

// Account contains the account information and the homes associated with it
type Account struct {
	ID    string `json:"id"`
	Name  string `json:"name"`
	Homes []Home `json:"homes"`
}

// Home contains information about a home
type Home struct {
	ID              int    `json:"id"`
	Name            string `json:"name"`
	DateTimeZone    string `json:"dateTimeZone"`
	TemperatureUnit string `json:"temperatureUnit"`
}

// Zone contains basic information about a zone
type Zone struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

// Overlay contains information about zone overlays
type Overlay struct {
	Type    string `json:"type"`
	Setting struct {
		Type        string      `json:"type"`
		Power       string      `json:"power"`
		Temperature Temperature `json:"temperature"`
	} `json:"setting"`
}

// ZoneState contains detailed information about a zone
type ZoneState struct {
	Link struct {
		State string `json:"state"`
	} `json:"link"`
	Mode       string `json:"tadoMode"`
	OpenWindow struct {
		DetectedTime           time.Time `json:"detectedTime"`
		DurationInSeconds      int32     `json:"durationInSeconds"`
		Expiry                 time.Time `json:"expiry"`
		RemainingTimeInSeconds int32     `json:"remainingTimeInSeconds"`
	} `json:"openwindow,omitempty"`
	Overlay          *Overlay `json:"overlay,omitempty"`
	SensorDataPoints struct {
		Humidity struct {
			Percentage float64   `json:"percentage"`
			Timestamp  time.Time `json:"timestamp"`
			Type       string    `json:"type"`
		} `json:"humidity"`
		InsideTemperature struct {
			Celsius    float64 `json:"celsius"`
			Fahrenheit float64 `json:"fahrenheit"`
			Precision  struct {
				Celsius    float64 `json:"celsius"`
				Fahrenheit float64 `json:"fahrenheit"`
			} `json:"precision"`
			Timestamp time.Time `json:"timestamp"`
			Type      string    `json:"type"`
		} `json:"insideTemperature"`
	} `json:"sensorDataPoints"`
	Setting struct {
		Power       string      `json:"power"`
		Temperature Temperature `json:"temperature"`
		Type        string      `json:"type"`
	} `json:"setting"`
	Activitydatapoints struct {
		HeatingPower struct {
			Percentage  float64     `json:"percentage"`
			Temperature Temperature `json:"temperature"`
			Type        string      `json:"type"`
		} `json:"heatingPower"`
	} `json:"activityDataPoints"`
}

// HasOpenWindow returns 1 if a zone has an open window
func (s ZoneState) HasOpenWindow() float64 {
	if s.OpenWindow.DurationInSeconds > 0 {
		return 1
	}
	return 0
}

// HeatingPower returns the current heating power
func (s ZoneState) HeatingPower() float64 {
	return s.Activitydatapoints.HeatingPower.Percentage
}

// Humidity returns the current relative humidity
func (s ZoneState) Humidity() float64 {
	return s.SensorDataPoints.Humidity.Percentage
}

// IsAway returns 1 if all the users are away
func (s ZoneState) IsAway() float64 {
	if s.Mode == "AWAY" {
		return 1
	}
	return 0
}

// IsHeatingOn returns 1 if the heating is on
func (s ZoneState) IsHeatingOn() float64 {
	if s.Setting.Power == "ON" {
		return 1
	}
	return 0
}

// IsOnline returns 1 if a zone is online
func (s ZoneState) IsOnline() float64 {
	if s.Link.State == "ONLINE" {
		return 1
	}
	return 0
}

// IsOverlay returns 1 if an overlay (like manual heating) is set
func (s ZoneState) IsOverlay() float64 {
	if s.Overlay == nil {
		return 0
	}
	return 1
}

// TargetTemperature returns the target temperature
func (s ZoneState) TargetTemperature() float64 {
	return s.Setting.Temperature.Celsius
}

// TemperatureCelsius returns the current temperature in Celsius degrees
func (s ZoneState) TemperatureCelsius() float64 {
	return s.SensorDataPoints.InsideTemperature.Celsius
}

// Temperature contains the temperature expressed in two units
type Temperature struct {
	Celsius    float64 `json:"celsius,omitempty"`
	Fahrenheit float64 `json:"fahrenheit,omitempty"`
}

// Setting contains information about the current settings for a thermostat
type Setting struct {
	Type        string       `json:"type,omitempty"`
	Power       string       `json:"power,omitempty"`
	Mode        string       `json:"mode,omitempty"`
	FanSpeed    string       `json:"fanSpeed,omitempty"`
	Temperature *Temperature `json:"temperature,omitempty"`
}

// Termination contains information about the termination
type Termination struct {
	Type                   string     `json:"type,omitempty"`
	DurationInSeconds      int        `json:"durationInSeconds,omitempty"`
	RemainingTimeInSeconds int        `json:"remainingTimeInSeconds,omitempty"`
	Expiry                 *time.Time `json:"expiry,omitempty"`
	ProjectedExpiry        *time.Time `json:"projectedExpiry,omitempty"`
}

// Weather contains detailed information about a zone
type Weather struct {
	OutsideTemperature struct {
		Celsius    float64   `json:"celsius"`
		Fahrenheit float64   `json:"fahrenheit"`
		Timestamp  time.Time `json:"timestamp"`
		Type       string    `json:"type"`
		Precision  struct {
			Celsius    float64 `json:"celsius"`
			Fahrenheit float64 `json:"fahrenheit"`
		} `json:"precision"`
	} `json:"outsideTemperature"`
	SolarIntensity struct {
		Percentage float64   `json:"percentage"`
		Timestamp  time.Time `json:"timestamp"`
		Type       string    `json:"type"`
	} `json:"solarIntensity"`
	WeatherState struct {
		Value     string    `json:"value"`
		Timestamp time.Time `json:"timestamp"`
		Type      string    `json:"type"`
	} `json:"weatherState"`
}

// apiRequest queries the API and returns the result
func (e *Exporter) apiRequest(url string) ([]byte, error) {
	if err := auth.EnsureTokenIsValid(e.Ctx); err != nil {
		return nil, err
	}
	metrics.ClientRequests.Inc()
	res, err := e.Ctx.Client.Get(url)
	if err != nil {
		metrics.ClientErrors.WithLabelValues(fmt.Sprintf("fetch %s", url)).Inc()
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		metrics.ClientErrors.WithLabelValues(strconv.Itoa(res.StatusCode)).Inc()
		return nil, fmt.Errorf("return code: " + http.StatusText(res.StatusCode))
	}

	body, err := ioutil.ReadAll(res.Body)
	defer res.Body.Close()
	if err != nil {
		metrics.ClientErrors.WithLabelValues(fmt.Sprintf("decode %s", url)).Inc()
		return nil, err
	}
	return body, nil
}
