package tado_test

import (
	"bytes"
	"net/http"
	"regexp"
	"testing"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/common/expfmt"
	"gitlab.com/omame/tado-exporter/auth"
	"gitlab.com/omame/tado-exporter/tado"
)

func TestCollect(t *testing.T) {
	stubServer := stubTadoServer(t, "")
	defer stubServer.Close()
	exporter := &tado.Exporter{
		Ctx: &auth.Context{
			AuthURL:     stubServer.URL,
			Client:      &http.Client{},
			Credentials: credentials,
			LastRefresh: time.Now(),
		},
	}

	registry := prometheus.NewRegistry()
	if err := registry.Register(exporter); err != nil {
		t.Fatal(err)
	}

	mfs, err := registry.Gather()
	if err != nil {
		t.Fatal(err)
	}

	var buf bytes.Buffer
	for _, mf := range mfs {
		if _, err := expfmt.MetricFamilyToText(&buf, mf); err != nil {
			t.Fatal(err)
		}
	}

	for _, re := range []*regexp.Regexp{
		regexp.MustCompile(`\ntado_client_requests_total [0-9.]+`),
		regexp.MustCompile(`\ntado_home_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_open_window_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_open_window_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_outside_temperature_celsius\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_solar_intensity_percent\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_tokens_acquired_total [0-9.]+`),
		regexp.MustCompile(`\ntado_up [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_active_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_active_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_away_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_away_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_heating_percentage\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_heating_percentage\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_humidity_percent\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_humidity_percent\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_overlay_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_overlay_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_target_temperature_celsius\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_target_temperature_celsius\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_temperature_celsius\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_temperature_celsius\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_up\{.*\} [0-9.]+`),
		regexp.MustCompile(`\ntado_zone_up\{.*\} [0-9.]+`),
	} {
		if !re.Match(buf.Bytes()) {
			t.Errorf("want body to match %s\n%s", re, buf.String())
		}
	}
}
